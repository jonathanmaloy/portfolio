$(document).ready(function(){
	$('.menu-icon').click(function(){
		$(this).toggleClass('open');
		$('nav').toggleClass('active');	
		if ($('nav').hasClass('active')) {
			$('html').disableScroll();
		} else {
			$('html').enableScroll();
		}
	});
	Waves.init();
});
$(window).on('load', function() {
	$('#loading-container').delay(1000).fadeOut('slow');
});