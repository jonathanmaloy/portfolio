module.exports = function(grunt) {
	grunt.initConfig({	
		concat: {
			js: {
				src: [					
					"source/js/jquery-3.0.0.js",
					"source/js/waves.js",
					"source/js/initial.js",
					"source/js/main.js"
				],
				dest: 'prod/js/jm.js',
				options: {
					separator: ';'
				},
			},
			css: {
				src: [
					"source/css/normalize.css",
					"source/css/grid.css",
					"source/css/waves.css",
					"source/css/main.css"
				],
				dest: 'prod/css/jm.css'
			}
		},
	
		postcss: {
			options: {
				processors: [
					require('autoprefixer')({
						browsers: [
							'last 2 versions',
							'Chrome >= 30',
							'Firefox >= 30',
							'ie >= 10',
							'Safari >= 8']
					})
				]
			},
			expended: {
				src: 'prod/css/jm.css'
			}
		},
	
		cssmin: {
			options: {
				keepSpecialComments: 0
			},
			target: {
				files: [{
					expand: true,
					cwd: 'prod/css',
					src: ['*.css', '!*.min.css'],
					dest: 'prod/css',
					ext: '.min.css'
				}]
			}
		},
	
		uglify: {
			dist: {
				files: {
					'prod/js/jm.min.js': ['prod/js/jm.js']
				}
			}
		},
	
		usebanner: {
			release: {
				options: {
					position: 'top',
					banner: "/**\n * JM v"+ grunt.option( "newver" ) +" (http://www.jonathanmaloy.com)\n * Copyright 2016 Jonathan Maloy\n */",
					linebreak: true
				},
				files: {
					src: [ 'prod/css/*.css', 'prod/js/*.js']
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-banner');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.registerTask(
		'default',[
			'concat:js',
			'concat:css',
			'postcss:expended',
			'cssmin',
			'uglify:dist',
			'usebanner:release',
		]
	);
};